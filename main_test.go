package main

import (
	"image/png"
	"os"
	"testing"

	"github.com/matryer/is"
)

func TestStrToBits(t *testing.T) {
	is := is.New(t)
	type testCase struct {
		in  []byte
		out []byte
	}

	cases := []testCase{
		{[]byte{0x61}, []byte{0, 1, 1, 0, 0, 0, 0, 1}},
		{[]byte{0x61, 0x62}, []byte{0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0}},
		{[]byte{0x61, 0x62, 0x63}, []byte{0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1}},
	}

	for _, c := range cases {
		res := DataToBits(c.in)
		is.Equal(res, c.out)
	}
}

func TestBitsToStr(t *testing.T) {
	is := is.New(t)
	type testCase struct {
		in  []byte
		out []byte
	}

	cases := []testCase{
		{
			[]byte{0, 1, 1, 0, 0, 0, 0, 1},
			[]byte{0x61},
		},
		{
			[]byte{0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0},
			[]byte{0x61, 0x62},
		},
		{
			[]byte{0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1},
			[]byte{0x61, 0x62, 0x63},
		},
	}

	for _, c := range cases {
		res := BitsToData(c.in)
		is.Equal(res, c.out)
	}
}

func TestEncode(t *testing.T) {
	is := is.New(t)

	input := []byte{0xAA, 0xAA, 0xAA}
	expected := [][]byte{
		{0x1, 0x0, 0x1},
		{0x0, 0x1, 0x0},
		{0x1, 0x0, 0x1},
		{0x0, 0x1, 0x0},
		{0x1, 0x0, 0x1},
		{0x0, 0x1, 0x0},
		{0x1, 0x0, 0x1},
		{0x0, 0x1, 0x0},
		{0x0, 0x0, 0x0},
		{0x0, 0x0, 0x0},
		{0x0, 0x0, 0x0},
		{0x0, 0x0, 0x0},
	}

	inFile, err := os.Open("./test/blank.png")
	is.NoErr(err)
	defer inFile.Close()

	inImg, err := png.Decode(inFile)
	is.NoErr(err)

	outImg := Encode(inImg, input)

	actual := [][]byte{}
	for i := 0; i < 12; i++ {
		x := i % 16
		y := i / 16
		r, g, b, _ := outImg.At(x, y).RGBA()
		actual = append(actual, []byte{byte(r), byte(g), byte(b)})
	}

	is.Equal(expected, actual)
}

func TestDecode(t *testing.T) {
	is := is.New(t)

	expected := []byte{0xAA, 0xAA, 0xAA}

	inFile, err := os.Open("./test/encoded.png")
	is.NoErr(err)
	defer inFile.Close()

	inImg, err := png.Decode(inFile)
	is.NoErr(err)

	actual := Decode(inImg)
	is.Equal(expected, actual)

}
