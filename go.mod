module gitlab.com/k1ngjulien/stego

go 1.15

require (
	github.com/matryer/is v1.4.0
	github.com/urfave/cli v1.22.5
	github.com/urfave/cli/v2 v2.3.0
)
