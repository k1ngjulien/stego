package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
	"reflect"

	"github.com/urfave/cli/v2"
)

type Changeable interface {
	Set(x, y int, c color.Color)
}

func main() {
	app := &cli.App{
		Name:  "stego",
		Usage: "cli tool to hide and recover data in pngs",
		Commands: []*cli.Command{
			{
				Name:    "encode",
				Aliases: []string{"enc"},
				Usage:   "encode text into a file",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "input",
						Usage:    "Input image",
						Required: true,
						Aliases:  []string{"i"},
					},
					&cli.StringFlag{
						Name:     "output",
						Usage:    "Output image",
						Required: true,
						Aliases:  []string{"o"},
					},
					&cli.StringFlag{
						Name:     "message",
						Usage:    "Message to encode",
						Required: true,
						Aliases:  []string{"m"},
					},
				},
				Action: func(c *cli.Context) error {
					err := runEncode(
						c.String("input"),
						c.String("output"),
						c.String("message"),
					)

					return err
				},
			},
			{
				Name:    "decode",
				Aliases: []string{"dec"},
				Usage:   "decode data from encoded png",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "input",
						Usage:    "Input image",
						Required: true,
						Aliases:  []string{"i"},
					},
				},
				Action: func(c *cli.Context) error {
					err := runDecode(c.String("input"))
					return err
				},
			},
		},
	}

	err := app.Run(os.Args)

	if err != nil {
		log.Fatal(err)
	}
}

// DataToBits splits data bytes into an array of individual bits
// either 0 or 1
func DataToBits(input []byte) []byte {
	bits := []byte{}

	for _, char := range input {
		buffer := []byte{}
		for i := 0; i < 8; i++ {
			bit := byte(char & 0x1)
			buffer = append(buffer, bit)
			char >>= 1
		}

		reverseAny(buffer)
		bits = append(bits, buffer...)
	}
	return bits
}

// BitsToData combines an array of individual bits into a normal
// array of bytes
func BitsToData(input []byte) []byte {
	output := []byte{}

	var buffer byte = 0
	for i, bit := range input {
		buffer <<= 1
		buffer |= bit

		if i > 0 && (i+1)%8 == 0 {
			output = append(output, buffer)
			buffer = 0
		}

	}
	return output
}

func reverseAny(s interface{}) {
	n := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, n-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func runEncode(in, out, message string) error {
	input, err := os.Open(in)
	if err != nil {
		return err
	}
	defer input.Close()

	inImg, err := png.Decode(input)

	outImg := Encode(inImg, []byte(message))

	output, err := os.Create(out)
	if err != nil {
		return err
	}
	defer output.Close()

	err = png.Encode(output, outImg)
	if err != nil {
		return err
	}

	return nil
}

func Encode(img image.Image, data []byte) image.Image {
	size := img.Bounds().Max

	// append a null to mark end of data
	data = append(data, 0x0)

	// split data into individual bits
	bits := DataToBits(data)

	// append 0 bits until there's a round number of pixels
	// being set
	missingBitCount := 3 - (len(bits) % 3)
	for i := 0; i < missingBitCount; i++ {
		bits = append(bits, byte(0))
	}

	// get a changeable version of the image
	if cimg, ok := img.(Changeable); ok {
		numOfPixels := len(bits) / 3

		for i := 0; i < numOfPixels; i++ {
			x := i % size.X
			y := i / size.X
			p := img.At(x, y)
			r, g, b, a := p.RGBA()
			// clear lsb
			rb := uint8(r) & 0b11111110
			gb := uint8(g) & 0b11111110
			bb := uint8(b) & 0b11111110
			// set lsb
			rb |= bits[i*3]
			gb |= bits[i*3+1]
			bb |= bits[i*3+2]

			cimg.Set(x, y, color.RGBA{rb, gb, bb, uint8(a)})
			p = img.At(x, y)
		}
	}

	return img
}

func runDecode(in string) error {
	input, err := os.Open(in)
	if err != nil {
		return err
	}
	defer input.Close()

	inImg, err := png.Decode(input)

	text := Decode(inImg)

	fmt.Println(string(text))
	return nil
}

func isNull(bits []byte) bool {
	for _, b := range bits {
		if b != 0 {
			return false
		}
	}

	return true
}

func Decode(img image.Image) []byte {
	size := img.Bounds().Max

	bits := []byte{}

	// get bits
	// collect bits until you get a 0x0 byte
	pixelcount := 0
loop:
	for y := 0; y < size.Y; y++ {
		for x := 0; x < size.X; x++ {
			r, g, b, _ := img.At(x, y).RGBA()
			rb := uint8(r) & 0x1
			gb := uint8(g) & 0x1
			bb := uint8(b) & 0x1

			bits = append(bits, rb, gb, bb)
			pixelcount++

			if pixelcount > 0 && pixelcount%3 == 0 {
				if isNull(bits[len(bits)-8:]) {
					break loop

				}
			}
		}
	}

	// assemble bits into bytes
	data := BitsToData(bits)

	return data[:len(data)-1]
}
